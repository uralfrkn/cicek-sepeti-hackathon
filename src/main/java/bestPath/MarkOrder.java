package bestPath;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class MarkOrder {
	private Order[] orders;
	private Dailer[] dailers;
	
	public Order[] getOrders() {
		return orders;
	}


	public void setOrders(Order[] orders) {
		this.orders = orders;
	}



	public Dailer[] getDailers() {
		return dailers;
	}



	public void setDailers(Dailer[] dailers) {
		this.dailers = dailers;
	}



	private Distance[][] distances;

	
	public Distance[][] getDistances() {
		return distances;
	}



	public void setDistances(Distance[][] distances) {
		this.distances = distances;
	}



	MarkOrder(Distance[][] distances, Dailer[] d, Order[] o){
		this.setDistances(distances);
		this.setDailers(d);
		this.setOrders(o);
	}
	
	
	
	private LinkedHashMap<Order, Double> orderDistincesFor(int dailerIndex){
		Map<Order, Double> map = new HashMap<Order, Double>();
		for (int j = 0; j < distances[dailerIndex].length; j++) {
			Distance distance = distances[dailerIndex][j];
			map.put(distance.getOrder(), distance.getDistance());
		}
		
		LinkedHashMap<Order, Double> sorted = map.entrySet()
				.stream()
				.sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

		return sorted;
	}
	
	private List<Distance> orderDistances() {
        List<Distance> remainingDistances = new ArrayList<Distance>();
        
		for (int i = 0; i < dailers.length; i++) {
			for (int j = 0; j < orders.length; j++) {
				remainingDistances.add(distances[i][j]);
			}
		}
		remainingDistances.sort((d1, d2) -> Double.compare(d1.getDistance(), d2.getDistance()));
		
		return remainingDistances;
	}
	
	@SuppressWarnings("null")
	private List<Order> transferToAnotherDailer(){
		Dailer dailer = findAboveLimit();
		while(dailer != null) {
	        List<Distance> outOfRange= new ArrayList<Distance>();
	        
			for (int i = 0; i < dailers.length; i++) {
				for (int j = 0; j < orders.length; j++) {
					if( distances[i][j].getOrder().getSupplier() == dailer && distances[i][j].getDailer() != dailer )
						outOfRange.add(distances[i][j]);
				}
			}
			
			outOfRange.sort((d1, d2) -> Double.compare(d1.getDistance(), d2.getDistance()));

			int maxCount = dailer.getOrderCount() - dailer.getMaxLimit();
			for (Distance distance : outOfRange) {
				if( maxCount > 0 && distance.getDailer().getOrderCount() < distance.getDailer().getMaxLimit()) {
					dailer.decreaseOrderCount();
					distance.getDailer().increaseOrderCount();
					distance.getOrder().setSupplier(distance.getDailer(), distance.getDistance());
					maxCount--;
				}
			}
			
			dailer = findAboveLimit();
		}
		
		dailer = findBelowLimit();
		
		while(dailer != null) {
			List<Distance> outOfRange= new ArrayList<Distance>();

			for (int i = 0; i < dailers.length; i++) {
				for (int j = 0; j < orders.length; j++) {
					if( distances[i][j].getOrder().getSupplier() != dailer && distances[i][j].getDailer() != dailer )
						outOfRange.add(distances[i][j]);
				}
			}
			
			outOfRange.sort((d1, d2) -> Double.compare(d1.getDistance(), d2.getDistance()));
			
			int maxCount = dailer.getMaxLimit() - dailer.getOrderCount();
			for (Distance distance : outOfRange) {
				if( maxCount > 0 && distance.getDailer().getOrderCount() > distance.getDailer().getMinLimit() ) {
					dailer.increaseOrderCount();
					distance.getDailer().decreaseOrderCount();
					distance.getOrder().setSupplier(dailer, (new Distance(dailer, distance.getOrder())).getDistance());
					maxCount--;
				}
			}
			
			dailer = findBelowLimit();
		}
		
		List<Order> result = new ArrayList<Order>();
		
		for (Order order : orders) {
			result.add(order);
		}
		
		result.sort((o1, o2) ->  Integer.compare(o1.getSupplier().getId(), o2.getSupplier().getId()));
		
		return result;
	}
	
	private Dailer findBelowLimit() {
		for (Dailer dailer : dailers) {
			if ( dailer.getOrderCount() < dailer.getMinLimit() ) {
				return dailer;
			}
		}
		return null;
	}
	
	private Dailer findAboveLimit() {
		for (Dailer dailer : dailers) {
			if ( dailer.getOrderCount() > dailer.getMaxLimit() ) {
				return dailer;
			}
		}
		return null;
	}
	
	public void solve() {
		List<Distance> orderedDistances = orderDistances();
		
		for (Distance distance : orderedDistances) {
			if( distance.getOrder().getSupplier() == null ) {
				distance.getDailer().increaseOrderCount();
				distance.getOrder().setSupplier(distance.getDailer(), distance.getDistance());
			}
		}
		
		File directory = new File("result");
	    if (! directory.exists()){
	        directory.mkdir();
	    }
	    
	    List<Order> result = transferToAnotherDailer();
	    
		getMap(result);
		writeResultToTxtFile(result);
	}

	private void getMap(List<Order> result) {
		
		String baseUrl = "https://static-maps.yandex.ru/1.x/";
		String baseParameters = "?lang=tr_TR&size=650,450&z=13&l=map,trf&pt=";
		String url = baseUrl + baseParameters + mapMarkers(result);
		
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder()
				.url(url)
				.get()
				.addHeader("cache-control", "no-cache")
				.build();

		try {
			Response response = client.newCall(request).execute();
			
			InputStream in = response.body().byteStream();
			
			OutputStream out = new FileOutputStream("result/result.png");
			
			in.transferTo(out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void writeResultToTxtFile(List<Order> result) {
		try {
			File fout = new File("result/result.txt");
			FileOutputStream fos;
			fos = new FileOutputStream(fout);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			
			
			Dailer prev = result.get(0).getSupplier();
			
			bw.write( result.get(0).getSupplier().getName() + "bayinin dağıtım yapacağı sipariş numaraları");
			bw.newLine();
			
			for (Order order : result) {
				if(prev != order.getSupplier()) {
					bw.write( order.getSupplier().getName() + "bayinin dağıtım yapacağı sipariş numaraları");
					bw.newLine();
				}
				
				bw.write( Integer.toString(order.getId()) );
				bw.newLine();
				prev = order.getSupplier();
			}
		 
			bw.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String mapMarkers(List<Order> result) {
		String markers = String.join("~", result.stream().map(o -> o.marker()).collect(Collectors.toList()));
		
		List<String> dailerMarkers = new ArrayList<String>();
		
		for (Dailer dailer : dailers) {
			dailerMarkers.add(dailer.marker());
		}
		
		String dailerPlacemarkers = String.join("~", dailerMarkers);
		
		return markers;
	}
}














