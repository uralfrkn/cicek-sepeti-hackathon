package bestPath;

public class Distance {
	
	private Dailer dailer;
	private Order order;
	private double distance;
	
	Distance(Dailer d, Order o){
		this.setDailer(d);
		this.setOrder(o);
		this.calcDistince();
	}
	
	
	private void calcDistince() {
		this.distance = Math.sqrt(
				Math.pow(this.order.getPoint().getLatitude() - this.dailer.getPoint().getLatitude(), 2) +  
				Math.pow(this.order.getPoint().getLongitude() - this.dailer.getPoint().getLongitude(), 2)
				);
	}

	
	public Dailer getDailer() {
		return dailer;
	}
	public void setDailer(Dailer dailer) {
		this.dailer = dailer;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public double getDistance() {
		return distance;
	}
	
	public String toString() { 
		return "Distance between OrderId: " + order.getId() + " and DailerId: " + dailer.getId() + " is " + distance;
	} 
	
}
