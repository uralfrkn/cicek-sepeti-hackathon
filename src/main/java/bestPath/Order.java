package bestPath;

public class Order {
	private int id;
	private Point point;
	private Dailer supplier;
	private double distanceWithSupplier;
	
	Order(int id, Point p){
		this.setId(id);
		this.setPoint(p);
	}
	
	Order(int id, double d, double e){
		this.setId(id);
		this.setPoint(new Point(d, e));
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public Dailer getSupplier() {
		return supplier;
	}

	public void setSupplier(Dailer supplier, double distance) {
		this.supplier = supplier;
		this.distanceWithSupplier = distance;
	}
	
	public String toString() { 
		if (this.supplier == null ) {
			return "orderId: " + this.id;
		}
		return "orderId: " + this.id + " supplierId " + this.supplier.getId();
	}

	public double getDistanceWithSupplier() {
		return distanceWithSupplier;
	}
	
	public String marker() {
		return this.point.getLongitude() + "," + this.point.getLatitude() + "," + this.supplier.getOrderPlacemark();
	}

}
