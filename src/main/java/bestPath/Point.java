package bestPath;

public class Point {
	private double latitude;
	private double longitude;
	
	Point(double d, double e){
		this.setLatitude(d);
		this.setLongitude(e);
	}
	
	Point(Point p){
		this(p.latitude, p.longitude);
	}
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double d) {
		this.latitude = d;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double e) {
		this.longitude = e;
	}
}
