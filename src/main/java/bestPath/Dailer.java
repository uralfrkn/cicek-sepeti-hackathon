package bestPath;

public class Dailer {
	private String name;
	private int id;
	private Point point;
	private int minLimit;
	private int maxLimit;
	private int orderCount;
	private String placemark;
	private String orderPlacemark;
	
	public int getMinLimit() {
		return minLimit;
	}

	public void setMinLimit(int minLimit) {
		this.minLimit = minLimit;
	}

	public int getMaxLimit() {
		return maxLimit;
	}

	public void setMaxLimit(int maxLimit) {
		this.maxLimit = maxLimit;
	}

	Dailer(String name, int id, int minLimit, int maxLimit, Point p, String placemark, String orderPlacemark) {
		this.setName(name);
		this.setId(id);
		this.setPoint(p);
		this.setMinLimit(minLimit);
		this.setMaxLimit(maxLimit);
		this.orderCount = 0;
		this.setPlacemark(placemark);
		this.setOrderPlacemark(orderPlacemark);
	}
	
	Dailer(String name, int id, int minLimit, int maxLimit, double d, double e, String placemark, String orderPlacemark) {
		this(name, id, minLimit, maxLimit, new Point(d, e), placemark, orderPlacemark);
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Point getPoint() {
		return point;
	}
	public void setPoint(Point point) {
		this.point = point;
	}

	public int getOrderCount() {
		return orderCount;
	}
	
	public void increaseOrderCount() {
		this.orderCount++;
	}
	
	public void decreaseOrderCount() {
		this.orderCount--;
	}

	public String getPlacemark() {
		return placemark;
	}

	public void setPlacemark(String placemark) {
		this.placemark = placemark;
	}

	public String getOrderPlacemark() {
		return orderPlacemark;
	}

	public void setOrderPlacemark(String orderPlacemark) {
		this.orderPlacemark = orderPlacemark;
	}
	
	public String marker() {
		return this.point.getLongitude() + "," + this.point.getLatitude() + "," + this.placemark;
	}
	
	public String toString() { 
		return name + " id: " + id + " maxLimit: " + maxLimit + " minLimit: "  + minLimit + " current: " + orderCount;
	}
	
}
